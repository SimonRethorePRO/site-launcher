# Changelog

## 1.2.0 (2021-11-16)

### Added (4 changes)

- [Add shadow on card's titles](https://gitlab.com/SimonRethorePRO/site-launcher/-/commit/fc37dda63460a6a2ced0a4d15b0f45ae89ed6e76)
- [Manage Extranet link](https://gitlab.com/SimonRethorePRO/site-launcher/-/commit/c86e113adc315e15018ace2b0411aabc893c03fb)
- [Allow all links to have suffix possibility](https://gitlab.com/SimonRethorePRO/site-launcher/-/commit/e41d3c361a19b4380faa6c48af343d044d2bebb3)
- [Add lead developer ribbon](https://gitlab.com/SimonRethorePRO/site-launcher/-/commit/ff63b8f5bff1309d7a9bea8b7ec3ec0848e88e32)

### Fixed (1 change)

- [Fix README build command](https://gitlab.com/SimonRethorePRO/site-launcher/-/commit/8b0acf7942e9eb1b84c4fc320b9296ec63b9e004)

## 1.1.0 (2021-11-04)

### Added (2 changes)

- [Add icon + Update favicon](https://gitlab.com/SimonRethorePRO/site-launcher/-/commit/0293708f7ddf602c09aba79b067e4b2325009f82)
- [Add shadow on site's cards](https://gitlab.com/SimonRethorePRO/site-launcher/-/commit/19a292442be4ed0173def179b9af2afb4681c62b)