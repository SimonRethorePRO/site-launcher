clean:
	docker image rm site-launcher:latest node:11-alpine simonrethorepro/site-launcher:latest
install:
	docker build -t simonrethorepro/site-launcher . && \
	docker-compose build && \
	npm install
ip:
	docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' site-launcher
logs:
	docker-compose logs -f web
start:
	docker-compose up -d
stop:
	docker-compose down