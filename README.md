# Site Launcher

Site Launcher is a web application which has purpose to simplify the developer's life by saving their working web application's links under multiple environments.

## Project setup

First you can start by cloning the repository.

```bash
git clone https://gitlab.com/SimonRethorePRO/site-launcher.git
```

Then you can run locally:

```bash
npm install
```

For generating the build of the app to deploy it:

```bash
npm run build
```

## Usage

Add your data in the assets directory: 
- `/images` for the site's image
- `data.json` to define data that will be used by the app

```json
[
    {
        "name" : "Hello World",
        "image" : "images/hello-world.jpg",
        "lead": true,
        "environments": [
            { 
                "name": "Production", 
                "links": [
                    { 
                        "name": "Front-Office", 
                        "link": "https://hello-world.fr"
                    },
                    { 
                        "name": "Back-Office", 
                        "link": "https://hello-world.fr/admin"
                    },
                ] 
            },
            { 
                "name": "Local", 
                "links": [
                    { 
                        "name": "Front-Office", 
                        "link": "http://localhost:80"
                    },
                    { 
                        "name": "Back-Office", 
                        "link": "http://localhost:80/admin"
                    },
                ] 
            }
        ]
    },
    ...
]
```