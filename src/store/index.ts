import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    siteSelected: null,
    oldSiteSelected: null,
    environmentSelected: null
  },
  getters: {
    oldSiteSelected: state => {
      return state.oldSiteSelected
    },
    siteSelected: state => {
      return state.siteSelected
    },
    environmentSelected: state => {
      return state.environmentSelected
    }
  },
  mutations: {
    siteSelected (state, siteSelected) {
      state.oldSiteSelected = state.siteSelected
      state.siteSelected = siteSelected
    },
    environmentSelected (state, environmentSelected) {
      state.environmentSelected = environmentSelected
    }
  },
  actions: {
  },
  modules: {
  }
})
