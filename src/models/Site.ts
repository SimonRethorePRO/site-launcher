import { EnvironmentModel } from './Environment'

export interface SiteModel {
    name: string;
    image: string;
    environments: EnvironmentModel[];
}
