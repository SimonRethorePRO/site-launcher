import { LinkModel } from './Link'

export interface EnvironmentModel {
    name: string;
    links: LinkModel[];
}
